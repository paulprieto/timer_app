// import/require packages
const express = require("express");
const mongoose = require("mongoose");

// initialize app
const app = express();
const port = 4000;

//Create a database in your atlas.
//Put the credentials in the code below

//connect to mongoose
// username: b43_merng_db
// Password: N6VSYcNm2zvbOiwH

mongoose.connect(
	"mongodb+srv://b43_merng_db:N6VSYcNm2zvbOiwH@cluster0-idqso.mongodb.net/timer_app?retryWrites=true&w=majority",
	{
		useNewUrlParser: true
	}
);

// Console.log to know if you are connected to mongoDB
mongoose.connection.once("open", () => {
	console.log("Connected to mongoDB.");
});

// Initialize apollo server
const server = require("./queries/queries");

// Serve the app using apolloServer
server.applyMiddleware({
	app,
	path: "/timer_app"
});

//create a listener to log if you are connected
app.listen(port, () => {
	console.log(`🚀  Server ready at localhost:${port}${server.graphqlPath}`);
});

// this will not work if there is no "./queries/queries" directory.
// next step is to create it
