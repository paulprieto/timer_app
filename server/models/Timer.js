const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const timerSchema = new Schema(
	{
		endTime: {
			type: Date,
			required: false
		},
		remainingTime: {
			type: Number,
			required: true
		}
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Timer", timerSchema);
