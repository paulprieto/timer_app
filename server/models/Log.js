const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const logSchema = new Schema(
	{
		startOrStop: {
			type: String,
			required: false
		},
		remainingTime: {
			type: Number,
			required: true
		},
		timerId: {
			type: String,
			required: true
		},
		createdAt: {
			type: Date
		}
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Logs", logSchema);
