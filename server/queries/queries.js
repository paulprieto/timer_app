const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");

const Timer = require("../models/Timer");
const Log = require("../models/Log");

const customScalarResolver = {
	Date: GraphQLDateTime
};

const typeDefs = gql`
	# this is a comment
	# the type query is the root of all GraphQL queries
	# this is used for executing GET requests

	scalar Date

	type TimerType {
		id: ID
		endTime: Date
		remainingTime: Int!
		logs: [LogType]
	}

	type LogType {
		id: ID
		timerId: ID!
		startOrStop: String!
		remainingTime: Int!
	}

	type Query {
		getTimers: [TimerType]
		getTimer(id: ID!): TimerType
	}

	type Mutation {
		createTimer(remainingTime: Int!): TimerType
		createLog(
			timerId: ID!
			remainingTime: Int!
			startOrStop: String!
		): LogType

		deleteLog(id: ID!): Boolean

		#stopTimer(id: ID!): TimerType
		#resumeTimer(id: ID!): TimerType

		#deleteTimer(id: ID!): Boolean
	}
`;

const resolvers = {
	// resolver are what are we going to return when the query is executed

	// Include queries inside Query
	Query: {
		getTimers: () => {
			return Timer.find({});
		},

		getTimer: (_, args) => {
			return Timer.findById(args.id);
		}
	},

	// Include mutations inside Mutation
	Mutation: {
		createTimer: (_, args) => {
			console.log("creating timer...");
			console.log(args);

			let newTimer = new Timer({
				remainingTime: args.remainingTime
			});

			return newTimer.save().then(timer => {
				// console.log(timer);
				let newLog = new Log({
					timerId: timer.id,
					remainingTime: timer.remainingTime,
					startOrStop: "Initial"
				});

				newLog.save();
			});
		},

		createLog: (_, args) => {
			console.log("creating timer log...");
			console.log(args);

			let newLog = new Log({
				timerId: args.timerId,
				remainingTime: args.remainingTime,
				startOrStop: args.startOrStop
			});

			return newLog.save();
		},

		deleteLog: (_, args) => {
			console.log("Deleting Log...");
			console.log(args);

			return Log.findByIdAndDelete(args.id).then((log, err) => {
				console.log(err);
				console.log(log);
				if (err || !log) {
					console.log("delete failed.");
					return false;
				}

				console.log("Log deleted");
				return true;
			});
		}
		// stopTimer: (_, args) => {
		// 	console.log("stopping timer...");
		// 	console.log(args);

		// 	return Booking.findById(args.id).then(timer => {
		// 		console.log(timer);
		// 	});
		// },

		// // Sample Mutation
		// createMember: (_, args) => {
		// 	let newMember = new Member({
		// 		firstName: args.firstName,
		// 		lastName: args.lastName,
		// 		position: args.position,
		// 		teamID: args.teamID,
		// 		//Syntax: bcrypt(plain password, salt rounds)
		// 		password: bcrypt.hashSync(args.password, 10),
		// 		username: args.username
		// 	});

		// 	console.log("Creating a member...");
		// 	console.log(args);
		// 	return newMember.save();
		// },

		// updateMember: (_, args) => {
		// 	console.log("Updating member info...");
		// 	console.log(args);

		// 	let condition = { _id: args.id };
		// 	let updates = {
		// 		firstName: args.firstName,
		// 		lastName: args.lastName,
		// 		position: args.position,
		// 		teamID: args.teamID,
		// 		password: args.password,
		// 		username: args.username
		// 	};

		// 	return Member.findOneAndUpdate(condition, updates);
		// },

		// deleteMember: (_, args) => {
		// 	console.log("Deleting Member...");
		// 	console.log(args);

		// 	return Member.findByIdAndDelete(args.id).then((member, err) => {
		// 		console.log(err);
		// 		console.log(member);
		// 		if (err || !member) {
		// 			console.log("delete failed.");
		// 			return false;
		// 		}

		// 		console.log("Member deleted");
		// 		return true;
		// 	});
		// }
	},

	// custom resolvers.
	TimerType: {
		// Sample resolver
		logs: (parent, args) => {
			return Log.find({ timerId: parent.id });
		}
	}
};

// Pass the typedefs and resolvers to a variable
// This will be used to communicate with App.js
const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
