import React from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { Container } from "react-bulma-components";

import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-bulma-components/dist/react-bulma-components.min.css";

import Timers from "./components/Timers";
import AddTimer from "./components/AddTimer";

const client = new ApolloClient({
	uri: "http://localhost:4000/timer_app"
});

const App = props => {
	// console.log(props);
	return (
		<ApolloProvider client={client}>
			<Container className="mt-3">
				<AddTimer />
				<Timers />
			</Container>
		</ApolloProvider>
	);
};

export default App;
