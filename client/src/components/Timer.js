import React, { useState } from "react";
import { graphql } from "react-apollo";
import { Columns, Heading, Button, Card, Table } from "react-bulma-components";
import moment from "moment";
import { flowRight as compose } from "lodash";
import { useStopwatch } from "react-timer-hook";
import Timer from "react-compound-timer";

import { createLogMutation, deleteLogMutation } from "./../queries/mutations";
import { getTimersQuery } from "./../queries/queries";

const Timerx = props => {
	// console.log(props);
	// let [timer, setTimer] = useState(3);
	// let [val, setVal] = useState(0);
	// let [play, setPlay] = useState(false);
	// let [disable, setDisable] = useState(false);
	let [mySeconds, setMySeconds] = useState(0);
	let [myTimerState, setMyTimerState] = useState();

	let timerLogs = props.logs ? props.logs : [];
	let rmt =
		typeof timerLogs !== undefined
			? timerLogs[timerLogs.length - 1].remainingTime
			: 0;

	// console.log(mySeconds);
	// console.log(rmt);

	// const playChangeHandler = () => {
	// 	setPlay(!play);
	// };
	// console.log(myTimerState);

	useStopwatch({ autoStart: false });

	return (
		<Columns.Column size={6}>
			<Columns>
				<Columns.Column size={6}>
					<Card className="p-2">
						<Card.Header className="mb-4">
							<Card.Header.Title
								className="is-centered"
								style={{ fontSize: "2rem" }}
							>
								Timer
							</Card.Header.Title>
						</Card.Header>
						<Timer
							initialTime={rmt * 1000}
							lastUnit="m"
							startImmediately={false}
							direction="backward"
							onStart={() => {
								let newLog = {
									timerId: props.timerid,
									remainingTime: parseInt(mySeconds),
									startOrStop: "start"
								};

								props.createLogMutation({
									variables: newLog,
									refetchQueries: [{ query: getTimersQuery }]
								});
							}}
							onPause={() => {
								let newLog = {
									timerId: props.timerid,
									remainingTime: parseInt(mySeconds),
									startOrStop: "stop"
								};

								props.createLogMutation({
									variables: newLog,
									refetchQueries: [{ query: getTimersQuery }]
								});
							}}
							onStop={() => {}}
							onReset={() => console.log("onReset hook")}
						>
							{({
								start,
								resume,
								pause,
								stop,
								reset,
								timerState
							}) => (
								<React.Fragment>
									<Heading className="has-text-centered">
										<Timer.Minutes />{" "}
										{mySeconds >= 120
											? "MINUTES"
											: "MINUTE"}
										<br />
										<Timer.Seconds />{" "}
										{Timer.Seconds()._owner.memoizedState
											.s >= 1
											? "SECONDS"
											: "SECOND"}
										{setMySeconds(
											Timer.Seconds()._owner.memoizedState
												.m *
												60 +
												Timer.Seconds()._owner
													.memoizedState.s
										)}
										{setMyTimerState(
											Timer.Seconds()._owner.memoizedState
												.state
										)}
									</Heading>
									<br />
									<Button.Group className="has-text-centered p-4">
										<Button
											onClick={start}
											color={"primary"}
										>
											Start
										</Button>
										<Button
											onClick={pause}
											color={"warning"}
										>
											Pause
										</Button>

										<Button onClick={reset} color={"dark"}>
											Reset
										</Button>
									</Button.Group>
								</React.Fragment>
							)}
						</Timer>
					</Card>
				</Columns.Column>
				<Columns.Column size={6}>
					<Card className="p-2">
						<Card.Header className="mb-4">
							<Card.Header.Title
								className="is-centered"
								style={{ fontSize: "2rem" }}
							>
								Logs
							</Card.Header.Title>
						</Card.Header>
						<Table>
							<thead>
								<tr>
									<th className="has-text-centered">Type</th>
									<th className="has-text-centered">
										Remaining Time
									</th>
									<th className="has-text-centered">
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								{timerLogs
									.map(log => {
										// console.log(log);
										return (
											<tr key={log.id}>
												<td className="has-text-centered">
													{log.startOrStop}
												</td>
												<td className="has-text-centered">
													{moment
														.utc(
															log.remainingTime *
																1000
														)
														.format("HH:mm:ss")}
												</td>
												<td className="has-text-centered">
													{log.startOrStop !==
													"Initial" ? (
														<Button
															disabled={
																myTimerState ===
																"PLAYING"
																	? true
																	: false
															}
															color={"danger"}
															onClick={() => {
																props.deleteLogMutation(
																	{
																		variables: {
																			id:
																				log.id
																		},
																		refetchQueries: [
																			{
																				query: getTimersQuery
																			}
																		]
																	}
																);
																setMySeconds(
																	rmt
																);
															}}
														>
															Del
														</Button>
													) : (
														<Button>delete</Button>
													)}
												</td>
											</tr>
										);
									})
									.reverse()}
							</tbody>
						</Table>
					</Card>
				</Columns.Column>
			</Columns>
		</Columns.Column>
	);
};

export default compose(
	graphql(createLogMutation, { name: "createLogMutation" }),
	graphql(deleteLogMutation, { name: "deleteLogMutation" })
)(Timerx);
