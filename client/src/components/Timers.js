import React from "react";
import { graphql } from "react-apollo";
import { Columns } from "react-bulma-components";

import { getTimersQuery } from "./../queries/queries";
import Timer from "./Timer";

const Timers = props => {
	// console.log(props);

	let timersData = props.getTimersQuery.getTimers
		? props.getTimersQuery.getTimers
		: [];

	return (
		<Columns>
			{timersData.map(timer => {
				// console.log(timer);
				return (
					<Timer
						duration={timer.remainingTime}
						key={timer.id}
						timerid={timer.id}
						logs={timer.logs}
					/>
				);
			})}
		</Columns>
	);
};

export default graphql(getTimersQuery, { name: "getTimersQuery" })(Timers);
