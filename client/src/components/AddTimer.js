import React, { useState } from "react";
import { Heading, Button, Box, Columns } from "react-bulma-components";
import { graphql } from "react-apollo";

import { getTimersQuery } from "./../queries/queries";
import { createTimerMutation } from "./../queries/mutations";

const AddTimer = props => {
	const [minutes, setMinutes] = useState(2);
	const [seconds, setSeconds] = useState(0);

	const minuteChangeHandler = e => {
		setMinutes(e.target.value);
	};

	const secondChangeHandler = e => {
		setSeconds(e.target.value);
	};

	const addTimer = e => {
		e.preventDefault();
		let rmt = 0;
		console.log(minutes);
		console.log(seconds);
		if (minutes >= 0) {
			rmt += minutes * 60;
		}
		rmt += seconds;

		let newTimer = {
			remainingTime: parseInt(rmt)
		};

		props.createTimerMutation({
			variables: newTimer,
			refetchQueries: [{ query: getTimersQuery }]
		});
	};
	return (
		<Columns>
			<Columns.Column size={6}>
				<Box>
					<Heading>Add Timer</Heading>
					<form onSubmit={addTimer}>
						<div className="field">
							<label className="label" htmlFor="minute">
								Minutes
							</label>
							<input
								id="minute"
								className="input"
								type="number"
								min="0"
								value={minutes}
								onChange={minuteChangeHandler}
								style={{ width: 200 }}
							/>
						</div>
						<div className="field">
							<label className="label" htmlFor="second">
								Seconds
							</label>
							<input
								id="second"
								className="input"
								type="number"
								min="0"
								value={seconds}
								onChange={secondChangeHandler}
								style={{ width: 200 }}
							/>
						</div>
						<Button color={"primary"}>Create Timer</Button>
					</form>
				</Box>
			</Columns.Column>
		</Columns>
	);
};

export default graphql(createTimerMutation, { name: "createTimerMutation" })(
	AddTimer
);
