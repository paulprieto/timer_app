import { gql } from "apollo-boost";

const getTimersQuery = gql`
	{
		getTimers {
			remainingTime
			id
			logs {
				id
				timerId
				remainingTime
				startOrStop
			}
		}
	}
`;

export { getTimersQuery };
