import { gql } from "apollo-boost";

const createLogMutation = gql`
	mutation($timerId: ID!, $remainingTime: Int!, $startOrStop: String!) {
		createLog(
			timerId: $timerId
			remainingTime: $remainingTime
			startOrStop: $startOrStop
		) {
			id
			timerId
			startOrStop
			remainingTime
		}
	}
`;

const deleteLogMutation = gql`
	mutation($id: ID!) {
		deleteLog(id: $id)
	}
`;

const createTimerMutation = gql`
	mutation($remainingTime: Int!) {
		createTimer(remainingTime: $remainingTime) {
			id
		}
	}
`;

export { createLogMutation, deleteLogMutation, createTimerMutation };
